package das.iker.labo1

import android.content.res.Configuration
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.Button
import androidx.compose.material.Divider
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import das.iker.labo1.ui.theme.Labo_1Theme


/*
Esta parte combina en uno los últimos 3 ejercicios del laboratorio 1 donde se pide:
1 - Ocultar mediante botones distintos elementos de la UI
2 - Cambiar entre 2 layouts mediante botones
3 - Ofrecer un textbox en la primera pantalla y emplear lo escrito en la segunda

La primera pregunta no tiene sentido en Compose, como se ha dicho al principio en Compose los elementos
son inmutables, una vez que se crean no cambian, no son Instancias de Objetos de PMOO. Si queremos cambiar
algo se RECOMPONE esa sección de la UI al cambiar un estado.
Por tanto no podemos "ocultar" un elemento, bueno, en cierto modo sí puedes pero ¿para qué ocultarlo
si simplemente puedes no volverlo a generar? A veces los objetos ocultos siguen teniendo efecto en
las interfaces y hay que programar tanto la lógica que define cuando ocultarlos Y cuando desocultarlos.

Además los datos no se pierden pues se guardan en un estado (recordemos que los elementos no tienen
atributos porque no son objetos).

De hecho en la parte 2 ya hemos jugado con la desaparición con los botónes:
- Según el color se renderizaba un tipo de botón u otro. Nunca había 2 a la vez, solo uno, el que
tenía que existir en ese momento.

La segunda pregunta se responde con los resultados de la primera. En Compose todo está escrito en funciones
que representan elementos Componibles (@Compose), por tanto, las distintas ventanas (layouts XML) son
como cualquier otro elemento. Por tanto podemos hacer 2 elementos componibles para las distintas ventanas
(PantallaPrincipal y PantallaSecundaria) y un tercero que decida cuál mostrar según un boolean (EjercicioParte3)

En este caso EjercicioParte3 genera un elemento SeccionLaboratorio por coherencia con el resto de partes
pero no es necesario y podríamos quitarlo si queremos delegar toda la pantalla a otro composable.

Ahora viene el problema ¿Cómo actualizamos la variable booleana que nos indica la pantalla a mostrar?
Recordemos que debemos recordar que los elementos han de ser idempotentes, es decir, nada de variables globales.
Además han de ser lo más genéricas posibles y por tanto no deben saber nada de la lógica del padre.
Es decir el botón de la pantalla inicial no tiene porqué saber que el elemento componible que lo ha
llamado (EjercicioParte3) quiere que cambie un booleano.

Para ello vamos a emplear funciones anónimas y en concreto funciones lambda:
Las funciones lambda son funciones anónimas que mantienen el contexto del lugar donde se han definido.
Es decir, si lo creamos en EjercicioParte3, la función lambda tiene acceso a todas las variables de
EjercicioParte3 y a los parámetros de EjercicioParte3 en caso de tener (que en este caso no tiene).

Luego podemos ejecutar esta función en otra función distinta, por ejemplo en el botón de la PantallaPrincipal
(para ello pasamos la función lambda por parámetro a PantallaPrincipal) y la lambda aunque lo hemos ejecutado
en PantallaPrincipal y no en EjercicioParte3 sigue teniendo acceso al contexto (variables, parámetros, etc.)
de EjercicioParte3. Ahora, hay que recordar que esta lambda no puede acceder a nada que haya definido
en PantallaPrincipal directamente. De hecho PantallaPrincipal no debería saber que hace esa lambda
y la lambda no debería estar pensada para ejecutarse con el contexto de PantallaPrincipal (esto es falso a medias).

Entonces, para el problema 1 de cambiar el booleano que selecciona la pantalla a mostrar:
- Creamos un estado que alberga un booleano indicando si mostrar la pantalla inicial o no.
(importante, una variable a secas no sirve porque queremos que la interfaz se actualice si el booleano cambia)
- Generamos un if según ese booleano para decidir que pantalla mostrar.
- A la función de cada pantalla le pasamos un lambda donde se cambia el valor de nuestro estado booleano;
este lambda se establece como la acción onClick de los botones.
(Recordar que para eso hay que añadir el parámetro para el lambda en PantallaPrincipal y PantallaSecundaria)

Y ya estaría; debido a los conceptos nuevos parece más complicado de lo que es, pero es muy fácil,
código a parte que controle elementos que están definidos en otra parte (XML o un vista.java)

Por último falta el texto, para ello hacemos lo mismo que con el booleano, pero claro, en el booleano
sabíamos qué valor tenía que tomar y lo definíamos en la lambda, pero no sabemos que nombre nos va a poner el
usuario. Bueno, para ello las lambdas aceptan parámetros sí queremos:
{param1, param2 ->
    función1(param1)
    funcion2(param1, param2)
}
Así que simplemente decimos que nos pasen el nombre por parámetro y listo (y por esto decía
que el que la lambda no estuviera pensada para el lugar donde se iba a ejecutar era falso a medias)

En Kotlin si la lambda no tiene parámetros o solo tiene 1 se puede omitir la parte param1, param2 ->
Si tiene un solo parámetro y decidimos omitir la definición de parámetros entonces el parámetro se llama
"it" por convenio.

A la hora de pasar el nombre a la PantallaSecundaria lo pasamos como parámetro normal.
También se ve que en la PantallaPrincipal también paso el nombre como parámetro, esto es para que si vamos atrás
se mantenga el nombre.
Si véis el código de la PantallaPrincipal veréis que hay un estado interno para guardar el texto del TextxField,
y os puede estar pasando por la cabeza: Pero si hemos guardado ese valor en un rememberSaveable porque
no se mantiene cuando volvemos hacia atrás???????
Pues porque eliminamos la PantallaInicial de la composición (UI) cuando decidimos solo mostrar la
PantallaSecundaria. En este caso lo hemos elegido nosotros, no el sistema al cambiar la configuración,
así que los estados guardados con remember y rememberSaveable se BORRAN. Y cuando volvemos atrás se vuelven
a generar de 0. Esto es muy útil y evita problemas en la que nos dejamos algo mal configurado o sin limpiar.

Más adelante en otros datos ya se trabajará sobre como mantener datos "permanentes".
Tanto remember como rememberSaveable guardan datos en el flujo de vida del elemento componible
(y este acaba cuando lo quitamos de la composición, aunque más adelante lo volvamos a componer).

IMPORTANTE:

Se os ha podido pasar por la cabeza pasar directamente por parámetro la referencia al estado "name" de
EjercicioParte3 a PantallaInicial, esto es muy muy muy mala praxis y genera acoplamiento y posibles
resultados aleatorios, hay que usar lambdas para ello.

De hecho compose nos cubre y no permite editar los estados pasados por parámetro (de hecho name si lo
pasamos como parámetro, pero solo como si fuera un mero string de solo lectura, lo que es correcto).
 */

@Composable
fun EjercicioParte3() {
    SeccionLaboratorio(sectionNumber = 3) {
        // Generamos los estados requeridos por las ventanas
        var name by rememberSaveable { mutableStateOf("") }
        var firstWindow by rememberSaveable { mutableStateOf(true) }

        // Seleccionamos la ventana a mostrar según el estado booleano
        if (firstWindow) {
            // A PantallaInicial le pasamos un lambda para que cambie el valor del booelano y de name
            // el booleano se establece siempre a false.
            // el name se define según el parámetro que se le pase a la lambda (it)
            PantallaInicial(name = name, onButtonClick = {
                name = it
                firstWindow = false
            })
        } else {
            PantallaSecundaria(
                name = name,
                // Pasamos un lambda para cambiar el booleano a true
                onButtonClick = { firstWindow = true }
            )
        }
    }
}

/*
La pantalla principal es un poco más compleja de lo que se pedía, pero así se pueden ver mejor algunos
conceptos.

La parte extra es que el botón solo estará habilitado si el texto en el TextBox no está vacío. Además
después de la primera interacción del usuario el TextField se pondrá rojo marcando que hay un error.

Pero comencemos explicando los parámetros:
- [name] que es un string y nos marca el valor inicial que debería tener el textbox; por defecto vacío
- [onButtonClick] función que toma un parámetro de tipo string y devuelve Unit (Unit es el equivalente a Void)
                  Usaremos esta función como la acción onClick del botón.
                  Recordemos que ese parámetro string es el valor de la caja de texto y dejaremos que la
                  función lambda haga con el lo que vea conveniente.

(En Kotlin las funciones son First Class Objects y se pueden pasar como parámetro o guardar en variables
y ejecutarlas más adelante)


Si os habéis fijado necesitamos un variable para guardar el valor del TextField. Recordemos que los
elementos no son instancias de objetos y por tanto NO TIENEN ATRIBUTOS ni pueden guardar datos,
y una vez que se generan no se pueden editar.
Por ello el TextField necesita que le digamos que valor tiene, además de decirle qué tiene que hacer
cada vez que el usuario cambie ese valor (cada vez que pulse una tecla, la borre etc).
Si os fijáis le pasamos al parámetro onValueChange un lambda donde lo primero que hacemos es actualizar el estado
nameFieldText, si no hacemos esto da igual lo que escribamos que no va a aparecer en el TextField.
Como en este onValueChange CAMBIAMOS un ESTADO todos los elementos componibles que dependan de este estado
son recompuestos (regenerados), entre ellos el TextField que toma el estado nameFieldText como parámetro "value"

Esto de que el TextField no tenga un atributo/método para recoger el valor del usuario y que lo tengamos
en una variable "externa" no parece muy intuitivo pero permite una flexibilidad increible:
- Imaginemos que no queremos que el usuario escriba espacios, no puedes deshabilitarle esa tecla,
y una alternativa sería que hagas una validación al acabar pero claro, eso fastidia mucho al usuario.
Otra alternativa es que sustituyas los espacios por barra-bajas pero claro, si se lo haces cuando le da
al OK el usuario quizá no se percata del cambio y quizá no quiere ese resultado, lo mejor sería hacerlo
según va escribiendo.
Para hacer esto en android View tendrías que generar un listener, añadirlo al TextField, en ese listener
coger el valor, editarlo y volver a settearlo.

Aquí simplemente añades un replace al actualizar la variable y listo. Por ejemplo aquí hacemos una
comprobación de validez del nombre y se hace al momento, por cada letra que el usuario teclee.

Además al ser un estado otros elementos definidos en este elemento composable que contiene el TextField
pueden acceder a él y editarlo si fuera necesario:
- Por ejemplo nuestro botón puede leerlo y usarlo directamente pues es un estado del elemento PantallaInicial
- Si llegamos a tener unos botónes para hacer traducción automática por ejemplo, estos botones pueden cambiar
el estado nameFieldText directamente sin preocuparse de qué elemento tiene el texto para hacer un getText()
y luego tener que hacer un setText. Y si tenemos un textfield y luego 3 campos de textos que lo muestran
en distintos colores? Es más fácil que meter la pata o que se te olvide alguno.

De esta forma defines un nuevo estado para guardar el valor delTextField y los elementos que lo necesiten
simplemente acceden a él y listo, los elementos componibles que lo empleen se actualizarán automáticamente
cuando este cambie sin necesidad de que nos acordemos de quién "tiene el texto", como acceder a él y
a quién hay que actualizar cuando el texto cambie.

Lo mismo pasa con el estado para la validación, se hace a momento y el botón se activa/desactiva solo
según el estado al momento. Sin necesidad de una lógica que diga: si edito el textfield y sucede X
tengo que bloquear/desbloquear el botón. Es más declarativo: si el nombre no es válido bloquéate.

En resumen, tenemos un elemento componible (PantallaInicial) totalmente independiente que permite ofrecer
un TextField y un botón con acción personalizable (a esta acción le pasaremos el valor del textfield)
que automáticamente bloquea el botón. El padre (EjercicioParte3) no sabe todo esto, tampoco le importa,
al padré solo le importa que le da un valor, un nombre, y listo.

Esta lógica del botón se queda dentro de este elemento y no afecta a nada más allá.

De hecho si hubieramos querido hacerlo más genérico podríamos hacer que recibiera dos funciones extra:
- Una para determinar un postprocesado del texto (sustituir espacios por barra-bajas)
- Otra para determinar la validez del texto, por ejemplo un validador de emails etc.
Para ello añadiríamos 2 parámetros a PantallaPrincipal y los llamaríamos al otro lado de la asignación en
las líneas 232 y 233.
 */
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun PantallaInicial(name: String = "", onButtonClick: (name: String) -> Unit) {
    SubsectionLaboratorio {
        // Estado para guardar el valor del textbox
        var nameFieldText by rememberSaveable { mutableStateOf(name) }
        // Estados para controlar si el nombre es válido y activar o desactivar  el botón en consecuencia
        var isNameInvalid by rememberSaveable { mutableStateOf(false) }
        var isFirstTime by rememberSaveable { mutableStateOf(nameFieldText.isBlank()) }

        // Para tener acceso al keyboard y hacer que el teclado no tape el TextField ni el botón
        val keyboardController = LocalSoftwareKeyboardController.current

        // TextField
        OutlinedTextField(
            // Valor del TextField
            value = nameFieldText,
            // Etiqueta del TextField. Marcamos que es obligatoria si el usuario ha metido un valor incorrecto
            label = { Text(if (isNameInvalid) "Nombre *" else "Nombre") },
            // Place Holder
            placeholder = { Text("Escribe tu nombre") },
            // Acción a realizar cuando el usuario cambie el valor
            onValueChange = {
                nameFieldText = it
                isNameInvalid = nameFieldText.isBlank()
                isFirstTime = false
            },
            keyboardActions = KeyboardActions(
                onDone = {
                    keyboardController?.hide()
                }
            ),
            isError = isNameInvalid,
            singleLine = true,
        )
        Divider(Modifier.padding(vertical = 10.dp))
        Button(
            onClick = { onButtonClick(nameFieldText) },
            enabled = !isNameInvalid && !isFirstTime
        ) {
            Text("Continuar")
        }
    }
}

/*
La Pantalla Secundaria es mucho más sencilla que la principal:
- Ponemos un elemento de texto personalizado con el nombre que nos pasan como parámetro
- Ponemos un divider
- Añadimos un botón que ejecuta el lambda que nos pasen en la acción onClick
 */
@Composable
fun PantallaSecundaria(name: String, onButtonClick: () -> Unit) {
    SubsectionLaboratorio {
        Text(text = "Bienvenido $name")
        Divider(Modifier.padding(vertical = 10.dp))
        Button(onClick = onButtonClick) {
            Text("Volver")
        }
    }
}

//-------------------------------------------------------------------------------------

@Preview(name = "PreviewEjercicioParte3-Light")
@Composable
fun PreviewEjercicioParte3() {
    Labo_1Theme {
        EjercicioParte3()
    }
}

@Preview(
    name = "PreviewPantallaPrincipal-Dark",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
)
@Preview(name = "PreviewPantallaPrincipal-Light")
@Composable
fun PreviewPantallaPrincipal() {
    Labo_1Theme {
        PantallaInicial {}
    }
}

@Preview(
    name = "PreviewPantallaSecundaria-Dark",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
)
@Preview(name = "PreviewPantallaSecundaria-Light")
@Composable
fun PreviewPantallaSecundaria() {
    Labo_1Theme {
        PantallaSecundaria(name = "Preview") {}
    }
}
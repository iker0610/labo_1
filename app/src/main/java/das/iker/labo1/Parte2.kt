package das.iker.labo1

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import das.iker.labo1.ui.theme.Labo_1Theme

@Composable
fun EjercicioParte2() {
    // Empleamos el elemento SeccionLaboratorio definido en MainActivity.kt
    SeccionLaboratorio(sectionNumber = 2) {
        Column(verticalArrangement = Arrangement.spacedBy(16.dp)) {
            BotonParte2_1()
            BotonParte2_2()
            BotonParte2_3()
            BotonCambiaColor()
        }
    }
}

/*
En este ejemplo se presenta un botón que tiene un estado interno para guardar el número de clicks.
Para ello se emplea remember y MutableStateOf que genera un estado. Es importante recordar que
las variables comunes no sirven para guardar datos que representen el estado de un Composable
y que indiquen cuando ha de actualizarse ese elemento.

La recomposición en Compose funciona mediante el patrón Observer de forma automática, y para eso
necesitamos estados observables. Además el remember sirve para que la función pueda recordar ese valor/estado
a través de distintas recomposiciones.

Si probáis a darle un par de clicks y a girar el móvil o cambiar de modo claro a oscuro vereis que se resetea.
Esto es debido a que remember recordará el estado siempre que el objeto no llegue a eliminarse de la composición
o árbol de elementos UI. Cuando giramos la pantalla o cambiamos el tema todo se vuelve a regenerar de 0
por lo que el estado guardado con remember  se elimina porque ese objeto ya no existe.

La expersión by que se ve es una característica de Kotlin que permite delegar los getters y setters
de una propiedad a otra clase. Curiosamente en las funciones no hay propiedades (solo en las clases (atributos)
y si se definen a nivel global), en las funciones todo son variables normales (sin getters ni setters)
y remember no es una clase es una función, pero bueno los de Jetpack Compose hacen magia y
se puede lograr emplear la expresión by importando:
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue (en caso de que sea un var y vayamos a editar su valor)
La alternativa sería:
val clickCount = remember { mutableStateOf(0) }
Y luego para acceder al valor emplear clickCount.value; Esto es más engorroso.
 */
@Composable
fun BotonParte2_1() {
    SubsectionConExplicacionLaboratorio("Botón que guarda su estado (mientras exista) con remember")
    {
        var clickCount by remember { mutableStateOf(0) }

        Button(onClick = { clickCount += 1 }) {
            Text(text = clickCount.toString())
        }
    }
}

/*
En este segundo caso hacemos lo mismo pero en vez remember usamos rememberSaveable .
Esto permite que el elemento UI mantenga su estado incluso despues de procesos que requieran
recrear todo el árbol como rotar la pantalla o cambiar el de modo (cambios de configuración).
 */
@Composable
fun BotonParte2_2() {
    SubsectionConExplicacionLaboratorio("Botón que guarda su estado (siempre) con rememberSaveable")
    {
        var clickCount by rememberSaveable { mutableStateOf(0) }

        Button(onClick = { clickCount += 1 }) {
            Text(text = clickCount.toString())
        }
    }
}

/*
En este caso hacemos el mismo ejercicio pero en vez de que el botón actualice y muestre el contador
hacemos que un text muestre el contador y el botón los actualice.
 */
@Composable
fun BotonParte2_3() {
    SubsectionConExplicacionLaboratorio("Botón que muestra el número de clicks en la label de al lado") {
        var clickCount by rememberSaveable { mutableStateOf(0) }
        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(text = "Nº Clicks: $clickCount")
            Button(onClick = { clickCount += 1 }) {
                Text("Click Me")
            }
        }
    }
}

/*
En este caso podemos ver que si definimos las variables antes de SubsectionConExplicacionLaboratorio
(antes las definiamos dentro de esta sección) podemos cambiar el diseño de elementos padres e hijos.
Aunque hay que tener mucho cuidado con las variables locales pues hay que tener en cuenta las propiedades
de Compose. Recordemos que cada vez que cualquiera de los estados (by remember ...) cambie
todos los elementos que empleen esa propiedad se REGENRAN, pero solo si es necesario.
 */
@Composable
fun BotonCambiaColor() {
    var primaryColor: Boolean by rememberSaveable { mutableStateOf(false) }

    val text: String = stringResource(
        if (primaryColor) R.string.background_color_button_text
        else R.string.primary_color_button_text
    )
    SubsectionConExplicacionLaboratorio(
        explicacion = "Boton que cambia el color de la subsección y su propio tipo de botón.",
        backgroundColor =
        if (primaryColor) MaterialTheme.colors.primary
        else MaterialTheme.colors.surface

    ) {
        if (primaryColor) {
            OutlinedButton(onClick = { primaryColor = !primaryColor }) {
                Text(text = text)
            }
        } else {
            Button(onClick = { primaryColor = !primaryColor }) {
                Text(text = text)
            }
        }
    }
}

//-------------------------------------------------------------------------------------

@Preview(
    name = "PreviewEjercicioParte2-Dark",
    uiMode = Configuration.UI_MODE_NIGHT_YES,
)
@Preview(name = "PreviewEjercicioParte2-Light")
@Composable
fun PreviewEjercicioParte2() {
    Labo_1Theme {
        EjercicioParte2()
    }
}
package das.iker.labo1

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import das.iker.labo1.ui.theme.Labo_1Theme


/*
Jetpack Compose es declarativo, es decir no has de generar ni editar la UI, simplemente definirla
y ella misma se irá generando.

En JetPack Compose cada elemento se define mediante elementos componibles (Con el decorator @Composable)
Estos elementos emiten elementos de UI. Y son funciones. (Por tanto todo va por código, 0 XML)

Los elementos Composables han de ser idempotentes, es decir, toman unos parámetros y llamando varias veces
al mismo elemento con los mismos parámetros HAN DE DAR el MISMO resultado.

A diferencia de en los paradigmas tradicionales (imperativos) en Compose no es posible tener un objeto
que represente la interfaz, no puedes tener una variable que tenga el TextBox del Login por ejemplo.
De hecho son inmutables, una vez que se crean no se pueden cambiar.

Entonces, ¿Cómo se actualizan? Pues volviendose a crear mediante la recomposición.
Cada vez que uno de los parámetros de entrada cambien el elemento Composable se recomprondá.
Jetpack Compose es muy eficiente con ello, y si un elemento no ha cambiado no se regenerá además de
que generar la IU es mucho mejor.

Estas carácterísticas haces que los elementos sean más fáciles de crear, de reutilizar y de testear.

Además estos elementos pueden tener estados internos, aunque esto se verá más adelante.


IMPORTANTE

Como se ve cada elemento es una función, pero al llamar a algunas funciones como Column tienen la
sección de parámetros (entre paréntesis) y luego una sección enttre corchetes donde definimos a los hijos.
Esto se llama Trailing Lambda (una carácteristica de Kotlin) y se puede emplear cuando el último
parámetro de una función es otra función.
 */
@Composable
fun EjercicioParte1(numTextos: Int = 4) {
    // Empleamos el elemento SeccionLaboratorio definido en MainActivity.kt
    SeccionLaboratorio(sectionNumber = 1) {
        // Añadimos tantos textos como nos hayan indicado
        repeat(numTextos) {
            TextoParte1()
        }

    }
}

/*
Por mostrar la modularidad he separado el texto en un elemento nuevo TextoParte1,
aunque en este caso podría sobrar pues solo añado un default .
(En Kotlin los parámetros funcionan como en Python, tienen nombre, y default si quieres.
Y al llamarlos puedes indicar los parámetros en el orden que quieras indicando su nombre).

Aprovecho para recordar que como los elementos composables de la UI son funciones no existe herencia.
Por ejemplo imaginemos que este elemento con este default es super útil, no heredo de Text y le añado el default
simplemente "COMPONGO" un nuevo elemento que tenga el default y el texto.

Esto se complica un poco con los Layout pues se pueden generar layouts custom a parte de column,
row, box y constrain con una lógica propia, pero ese caso es más avanzado.
Con los modifiers es muy raro que requiramos hacer nuestro propio Layout a no ser que sea algo muy fancy.
 */
@Composable
fun TextoParte1(text: String = "Texto de Prueba") {
    Text(text = text)
}


/*
    PREVIEWS

En Andoid View tenemos el XML para ver como son los elementos (menos los que ddefinamos en el código)
Pero en Jetpack Compose gracias a que los elementos son Idempotentes e independientes podemos generar
Previews para los elementos Componibles. Estos se pueden ver y analizar en el propio Android Studio
sin necesidad de instalar la app o activar el emulador. Además algunos cambios básicos como el espaciado
entre elementos (8.dp por ejemplo) se puede cambiar y ver el efecto instantáneamente sin compilar.

Además, en la misma función de la Preview podemos añadir más de una preview,
y pasarle parámetros al decorator @Preview (nombre, si queremos el modo oscuro, tamaño etc...)
Lo que viene muy bien para comprobar colores o si estamos haciendo algo responsive como se ajusta nuestros
elementos a los distintos tamaños.

Recordatorio: Los elementos componibles no tienen el tema aplicado, el tema se aplica en el Activity
al principio (linea 25 de MainActivity.kt) por lo que al instalar la app el tema se aplica a todo,
pero en las previews no, así que es necesario aplicar el tema correspondiente en las previews.
(En un tema MaterialTheme están incluidos de forma automática el tema claro y oscuro y se cambian automáticamente)
 */

@Preview(
    name = "PreviewEjercicioParte1-Dark",
    uiMode = UI_MODE_NIGHT_YES,
)
@Preview(name = "PreviewEjercicioParte1-Light")
@Composable
fun PreviewEjercicioParte1() {
    Labo_1Theme {
        EjercicioParte1()
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewTextoParte1() {
    Labo_1Theme {
        TextoParte1("Texto para la Preview")
    }
}


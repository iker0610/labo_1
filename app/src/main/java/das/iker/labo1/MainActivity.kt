package das.iker.labo1

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import das.iker.labo1.ui.theme.Labo_1Theme

// Definimos la actividdad
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // Aplicamos el tema
            Labo_1Theme {
                Laboratorio1()
            }
        }
    }
}

@Composable
fun Laboratorio1() {
    Column(
        Modifier
            .background(MaterialTheme.colors.background)
            .verticalScroll(rememberScrollState())
            .padding(vertical = 32.dp, horizontal = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        EjercicioParte1()
        EjercicioParte2()
        EjercicioParte3()
    }
}

@Preview(
    name = "PreviewLaboratorio1-Dark",
    widthDp = 320,
    heightDp = 1200,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
)
@Preview(name = "PreviewLaboratorio1-Light", widthDp = 320, heightDp = 1200)
@Composable
fun PreviewLaboratorio1() {
    Labo_1Theme {
        Laboratorio1()
    }
}


@Composable
fun SeccionLaboratorio(
    sectionNumber: Int,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Card(modifier.fillMaxWidth(), elevation = 5.dp) {
        /*
        En Compose hay 3 "layouts" por defecto: column, row, box
        Adicionalmente está el Constraint Layout aunque este no es tan necesario en Compose como
        lo es en Android View (XML), pues Jetpack Compose es mucho más eficiente y
        no tiene sobrecarga si usas varias columnas/filas anidadas como pasa en Android View
        */
        Column(
            // Los Modifier son un aspecto muy importante y potente en Compose, pero ahora no es importante
            // Como primera aproximación podemos pensar en ellos como los atributos en el XML.
            Modifier.padding(all = 16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            // Añadimos un texto a la sección
            Text(
                text = "Parte $sectionNumber",
                style = MaterialTheme.typography.h5
            )
            // Un divisor
            Divider(Modifier.padding(vertical = 10.dp))
            // Y por último el contenido que nos pasen
            content()
        }
    }
}

@Composable
fun SubsectionLaboratorio(
    backgroundColor: Color = MaterialTheme.colors.surface,
    content: @Composable () -> Unit
) {
    Card(elevation = 3.dp, backgroundColor = backgroundColor) {
        Column(
            Modifier.padding(all = 16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            content()
        }
    }
}

@Composable
fun SubsectionConExplicacionLaboratorio(
    explicacion: String,
    backgroundColor: Color = MaterialTheme.colors.surface,
    content: @Composable () -> Unit
) {
    SubsectionLaboratorio(backgroundColor = backgroundColor) {
        Text(
            text = explicacion,
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center
        )
        Divider(
            Modifier.padding(vertical = 10.dp)
        )
        content()
    }
}